use strict;
use warnings;


my $wort = <STDIN>;
my %statistic = ();
chomp $wort;


while(<>){
	my @zeile = split( / /, $_);
	
	foreach my $neueswort (@zeile){
		if (exists($statistic{$neueswort})){
			$statistic{$neueswort} = my $K++;
		}else{
			$statistic{$neueswort} = 0;
		}
		
	}
}

#https://www.tutorialspoint.com/perl/perl_hashes.htm
#http://stackoverflow.com/questions/95820/in-perl-how-do-i-create-a-hash-whose-keys-come-from-a-given-array

#print "Das Ergebnis fuer \"$wort\" lautet:$anzahl.";